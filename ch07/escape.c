#include <stdio.h>
#include <stdlib.h>
#define _GNU_SOURCE
#include<getopt.h>

static void do_cat(FILE *f);
static void do_catc(FILE *f);

static struct option longopts[] = {
  {"visualize",no_argument,NULL,'v'},
  {0,0,0,0}
};

int main(int argc, char *argv[])
{
  if(argc > 3){
      fprintf(stderr,"Usage: %s file [-v]");
      exit(1);
  }
  
  int opt;
  int c;
  FILE *f = fopen(argv[1],"r");
  while((opt = getopt_long(argc,argv,"v",longopts,NULL))!= -1){
    switch(opt){
      case 'v':
        do_cat(f);
        fclose(f);
        break;
      case '?':
        fprintf(stderr,"Invalid option %s",argv[2]);
        exit(1);
      default:
        do_catc(f);
        fclose(f);
        break;
      }
    
    }
  exit(0);
}

static void do_cat(FILE *f)
{
  int c;

  while ((c = fgetc(f)) != EOF) {
    switch (c) {
      case '\t':
        if (fputs("\\t", stdout) == EOF) exit(1);
        break;
      case '\n':
        if (fputs("$\n", stdout) == EOF) exit(1);
        break;
      default:
        if (putchar(c) < 0) exit(1);
        break;
    }
  }
}

static void do_catc(FILE *f){
  int c;
  while((c = fgetc(f)) != EOF){
    if(putchar(c)<0) exit(1);
    }
    exit(0); 
 }
