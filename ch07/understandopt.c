#include<stdio.h>
#include<stdlib.h>
#define _GNU_SOURCE
#include<getopt.h>

static struct option longopts[] = {
  {"c",no_argument,NULL,'c'},
  {"python",required_argument,NULL,'p'},
  {"Ruby",no_argument,NULL,'r'},
  {0,0,0,0}
};

int main(int argc,char *argv[]){
  int opt;
  int parameter;
  while((opt = getopt_long(argc,argv,"cp:r",longopts,NULL)) != -1){
    switch(opt){
      case 'c':
        fprintf(stdout,"exploring %s in depth",longopts[0].name);
        exit(0);
      case 'p':
        parameter = atoi(optarg);
        fprintf(stdout,"exploring %s%d in depth",longopts[1].name,parameter);
        exit(0);
      case 'r':
        fprintf(stdout,"exploring %s in depth",longopts[2].name);
        exit(0);
      case '?':
        fprintf(stderr,"Invalid parameter");
        exit(1);
    }
    }
}
