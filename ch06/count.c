#include <stdio.h>
#include <stdlib.h>
#define BUFFER_SIZE 2048

static void do_count(FILE *f);

int main(int argc, char *argv[])
{
  if (argc == 1) {
    do_count(stdin);
  }
  else {
    int i;
    for (i = 1; i < argc; i++) {
      FILE *f = fopen(argv[i], "r");
      if (!f) {
        perror(argv[i]);
        exit(1);
      }
      do_count(f);
      fclose(f);
    }
  }
  exit(0);
}
static void do_count(FILE *f)
{
  unsigned char buf[BUFFER_SIZE];
  int maxlen = sizeof(buf);
  int count = 0;
  while (fgets(buf,maxlen,f) != NULL) {
    count++;
  }
  fprintf(stdout,"%d lines",count);
  exit(0);
}

