#include<stdio.h>
#include<stdlib.h>

int plus(int n);

int main(void){
  int(*f)(int);
  int result;

  f = plus;
  result = f(5);
  printf("%d\n",result);
  exit(0);
}

int plus(int n){
  return n + 1;
}
